import Vue from 'vue'
import VueRouter from 'vue-router'
// import index from '@/components/index/index'
import index from '@/index/index'
import BlogDetail from '@/index/blog/BlogDetail'
import MarkDownEditor from '@/components/utils/MarkDown'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: index
  },
  {
    path: '/markdown',
    name: 'markdown',
    component: MarkDownEditor
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "about" */ '../components/index/register.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "about" */ '../components/index/login.vue')
  },
  {
    path: '/blog',
    name: 'blogIndex',
    component: () => import(/* webpackChunkName: "about" */ '../components/blog/About.vue')
  },
  {
    path: '/blogDetail',
    name: 'blogDetail',
    component: BlogDetail
  },
  {
    path: '/blogCreate',
    name: 'blogCreate',
    component: () => import(/* webpackChunkName: "about" */ '../components/blog/Create.vue')
  },
  // {
  //   path: '/home',
  //   name: 'Home',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      // setTimeout to prevent scroll top before route change
      setTimeout(function () {
        return { x: 0, y: 0 }
      }, 500);
    }
  }
})

export default router
