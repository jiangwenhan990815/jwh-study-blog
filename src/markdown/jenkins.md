@[TOC](Jenkins部署vue项目)
# 1、前期准备
## 1-1、java
[jenkins支持java版本内容](https://www.jenkins.io/doc/book/platform-information/support-policy-java/index.html)
[java17下载地址](https://www.oracle.com/java/technologies/downloads/#java17)
> 因为需要安装最新jenkins，java版本需要11、17、21版本，本机主要以17为主

```bash
# 将java安装包安装到/usr/local文件夹下
cd /usr/local
wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz 
tar -zxvf jdk-17_linux-x64_bin.tar.gz
vim  /etc/profile

# 配置java
export JAVA_HOME=/usr/local/jdk-17.0.10
export CLASSPATH=.:$JAVA_HOME/lib
export PATH=$PATH:$JAVA_HOME/bin:$JAVA_HOME/jre/bin

# 变量环境生效
source /etc/profile
java -version

# 出现下述情况安装成功
java version "17.0.10" 2024-01-16 LTS
Java(TM) SE Runtime Environment (build 17.0.10+11-LTS-240)
Java HotSpot(TM) 64-Bit Server VM (build 17.0.10+11-LTS-240, mixed mode, sharing)
```
## 1-2、jenkins部署
[jenkins中文官网](https://www.jenkins.io/zh/)
[jenkins centos最新下载地址](https://pkg.jenkins.io/redhat-stable/)
> ps：centos版本最好是7.9，腾讯云服务器上部署是7.6版本，会出现反复重启情况，到现在也没明白为什么，求大佬。
> 

```bash
# 所有命令直接从官网复制就行，java-17需要自行安装
  sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
  sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io-2023.key
  yum install fontconfig java-17-openjdk
  yum install jenkins
# 查看jenkins文件
rpm -ql jenkins
# /usr/lib/systemd/system/jenkins.service
# 修改改文件配置
```
```txt
# 修改用户
User=root
# Group=jenkins

# 根据自己需求修改端口
Environment="JENKINS_PORT=8080"

# 修改java配置
Environment="JENKINS_JAVA_CMD=/usr/local/jdk-17.0.10/bin/java"
Environment="JAVA_HOME=/usr/local/jdk-17.0.10"
```
```bash
# 启动jenkins
systemctl start jenkins
# 查看jenkins
systemctl status jenkins
# 若修改配置，需重载一下
systemctl daemon-reload
# 大概率会遇见这种情况，这是因为第一次加载时间长，需要稍等会儿
Job for jenkins.service failed because a timeout was exceeded. See "systemctl status jenkins.service" and "journalctl -xe" for details.
# 当systemctl status jenkins这种情况下就启动成功了
● jenkins.service - Jenkins Continuous Integration Server
   Loaded: loaded (/usr/lib/systemd/system/jenkins.service; disabled; vendor preset: disabled)
   Active: active (running) since Fri 2024-02-23 23:30:52 CST; 2min 32s ago
 Main PID: 8749 (java)
   CGroup: /system.slice/jenkins.service
           └─8749 /usr/local/jdk-17.0.10/bin/java -Djava.awt.headless=true -jar /usr/share/java/jenkins.war --webroot=%C/jenkins/war --httpPort=8080

Feb 23 23:29:51 hcss-ecs-877a jenkins[8749]: This may also be found at: /var/lib/jenkins/secrets/initialAdminPassword
Feb 23 23:29:51 hcss-ecs-877a jenkins[8749]: *************************************************************
Feb 23 23:29:51 hcss-ecs-877a jenkins[8749]: *************************************************************
Feb 23 23:29:51 hcss-ecs-877a jenkins[8749]: *************************************************************
Feb 23 23:29:51 hcss-ecs-877a jenkins[8749]: 2024-02-23 15:29:51.115+0000 [id=49]        INFO        hudson.util.Retrier#start: Attempt #1 to do the action check updates server
Feb 23 23:30:52 hcss-ecs-877a jenkins[8749]: 2024-02-23 15:30:52.478+0000 [id=30]        INFO        jenkins.InitReactorRunner$1#onAttained: Completed initialization
Feb 23 23:30:52 hcss-ecs-877a jenkins[8749]: 2024-02-23 15:30:52.493+0000 [id=24]        INFO        hudson.lifecycle.Lifecycle#onReady: Jenkins is fully up and running
Feb 23 23:30:52 hcss-ecs-877a systemd[1]: Started Jenkins Continuous Integration Server.
Feb 23 23:32:11 hcss-ecs-877a jenkins[8749]: 2024-02-23 15:32:11.362+0000 [id=49]        INFO        h.m.DownloadService$Downloadable#load: Obtained the updated data file...enInstaller
Feb 23 23:32:11 hcss-ecs-877a jenkins[8749]: 2024-02-23 15:32:11.362+0000 [id=49]        INFO        hudson.util.Retrier#start: Performed the action check updates server ... attempt #1
Hint: Some lines were ellipsized, use -l to show in full.

# 第一次访问jenkins会有默认密码，查询密码即可,输入后等待稍长时间
cat /var/lib/jenkins/secrets/initialAdminPassword
```
> 根据自己的喜好自定义
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/3728d516a53542809e0e7cc081fab3c7.png)
点击安装后稍等一段时间就行啦
ps：有些安装失败的不要紧，可以后期去安装
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/630502e05c7e46c8905e167304361a76.png)
按照自己需求去设置
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/a54c7329414342dfbddb95dcb942bca2.png)
然后安装完成啦

## 1.3、安装node
>安装nodejs很简单
>[我安装的版本](https://nodejs.org/dist/v18.8.0/)

```bash
wget https://nodejs.org/dist/v18.8.0/node-v18.8.0-linux-x64.tar.gz   
tar -zxvf node-v18.8.0-linux-x64.tar.gz
ln -s /usr/local/node-v18.8.0-linux-x64/bin/node  /usr/local/bin/node
ln -s /usr/local/node-v18.8.0-linux-x64/bin/npm  /usr/local/bin/npm

#node
node: /lib64/libm.so.6: version `GLIBC_2.27' not found (required by node)
node: /lib64/libc.so.6: version `GLIBC_2.25' not found (required by node)
node: /lib64/libc.so.6: version `GLIBC_2.28' not found (required by node)
node: /lib64/libstdc++.so.6: version `CXXABI_1.3.9' not found (required by node)
node: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.20' not found (required by node)
node: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found (required by node)

# 这时候需要下一步操作
```
## 1.4、glibc、make、gcc更新
> 因为需要使用到node，所以需要更新glibc，一般情况下需要更新make，gcc和glibc(尽量安装以下版本组合，gcc版本过高过低无法编译glibc)
> make-4.3
> gcc-8
> glibc-2.28

```bash
# 升级make
wget --no-check-certificate https://ftp.gnu.org/gnu/make/make-4.3.tar.gz
tar -xzvf make-4.3.tar.gz
cd make-4.3v

# 安装到指定目录
./configure  --prefix=/usr/local/make
make
make install

# 创建软连接
cd /usr/bin/
mv make make.bak
ln -sv /usr/local/make/bin/make /usr/bin/make

# 查看是否更新
make -v
GNU Make 4.3
# 更新成功
Built for x86_64-pc-linux-gnu
Copyright (C) 1988-2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```
```bash
# 更新gcc
gcc -v
# 存在gcc的话需要删除
yum -y remove gcc g++
# 安装安装centos-release-scl
sudo yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/centos-release-scl-rh-2-3.el7.centos.noarch.rpm
sudo yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/centos-release-scl-2-3.el7.centos.noarch.rpm
# 安装devtoolset
# 如果安装其他版本，将devtoolset-8-gcc-c++改成devtoolset-<version>-gcc-c++即可
sudo yum install devtoolset-8-gcc-c++ 
# 如果需要可以安装其他版本，按照这个切换即可
scl enable devtoolset-8 bash
# 也可以使用这个命令
source /opt/rh/devtoolset-8/enable
# 查看gcc版本号变更
gcc --version
gcc (GCC) 8.3.1 20190311 (Red Hat 8.3.1-3)
Copyright (C) 2018 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

```bash
# 更新glibc
# 因为glibc要更新到2.28版本
# 下载并解压 glibc-2.28
wget --no-check-certificate https://ftp.gnu.org/gnu/glibc/glibc-2.28.tar.gz
tar -xzvf glibc-2.28.tar.gz
cd glibc-2.28

# 创建临时build文件
mkdir build && cd build
# 有时候在编译时可能会报错，大概率是gcc版本太高，有时候可能是缺少某个依赖，我在安装时缺少了bison依赖
yum install bison
# 编译
../configure --prefix=/usr --disable-profile --enable-add-ons --with-headers=/usr/include --with-binutils=/usr/bin
# make可以根据自己需求去跑多个编译命令make -j n,注意n不能超过本机CPU核心的2倍
make -j 4 && make install
# 查看glibc
strings /lib64/libc.so.6 | grep GLIBC
GLIBC_2.2.5
GLIBC_2.2.6
...
GLIBC_2.26
GLIBC_2.27
GLIBC_2.28
GLIBC_PRIVATE
```
>接下来就可以安心去搞jenkins啦
## 1.5、jenkins插件前期部署
> 由于需要远端服务器操作，需要安装ssh，和打包vue所需要的node
> 还需要在服务器上安装下git
> `yum install git`![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/eeaec22a1d1e47959b37c9581074e6a8.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/792c5029ef2d43fdb9f27d52cf0a0a04.png)

# 2、部署
## 2-1、前期配置
>因为需要用到远端服务器，所以需要配置远端服务器相关信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/e24b2203b0544c5d91e99ddecaeb31ad.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/3a06d23e75144f2b984443cb62f368bd.png)
这里直接选择密码登录
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/0f13c3609ab4417795dba5271dbcdc7e.png)
部署vue还需要node
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/8c10fee8a591483b85d52c9e09b7bf99.png)


## 2-2、新建任务
>在主页面选择新建item新建一个任务，如图选择第一个即可
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/c7121dc0657147739b1923bc91106701.png)
## 2-3、配置任务
>之后进入配置页面
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/ab7f39f9701b4a64bccfcc563083489e.png)
配置github上仓库（这个是我gitee上学习vue3的前端项目）
`https://gitee.com/jiangwenhan990815/jwh-study-blog.git`
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/01d90543073248b4ab5c644b921f0b39.png)
它会自动选择master分支，也可以根据自己要求选择其他分支
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/fee66a5777d3435f808e0fa5c386a37a.png)
之后构建环境选择以前设置的nodejs
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/9884b211bf474b679d6b392cf1266923.png)
构建环境还是选择shell命令，大多数还是这个
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/eb8703fad8c24a6eba76f1583a4c1ef7.png)
根据自己需求去执行命令
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/b280309e022d47458af66e9ed9e4b0d4.png)

```powershell
cd /var/lib/jenkins/workspace/vue-blog #进入blog项目目录
# npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver
npm install
npm run build
cd dist
rm -rf blog.tar.gz #删除上次打包生成的压缩文件
tar -zcvf blog.tar.gz * #把生成的项目打包成test方便传输到远程服务器
cd ../
```
>ps:这里要提一下jenkins的工作区，之前在这个方向踩过太多的坑，一般一个任务拉下来的git代码是会存在一个文件夹/itemName下，比如说我的jenkins的工作区是/var/lib/jenkins/workspace，我的项目名是vue-blog，所以该项目拉下来的git项目在/var/lib/jenkins/workspace/vue-blog 文件夹下，一般工作区在system里面可查看，一般不同版本的工作区间还是不同
>![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/2196fca4bc1b4506ab32a6e1c7e2ff31.png)
>构建后需要将包发送到服务器上
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/70ca69cc4cc64e10bfe2781ae7aab2a5.png)
编写配置，这里有个问题，需要上传到远端的文件夹是/usr/local/blog，但是它存到了/root/usr/local/blog，可以根据2-1前期配置去配置remove file
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/38b5177842334366bf50799a546d59c9.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/5d1aa41231de40e38ef53269a41c9876.png)
```bash
cd /root/usr/local/blog #进入远程服务器的blog目录
tar -zxvf blog.tar.gz -C /usr/local/blog/dist/ #解压blog文件到dist文件夹
rm -rf blog.tar.gz #删除blog文件
```

## 2-4、运行
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/7307fdde2e3242f8b2623be2704a5124.png)
可以通过该地方实时查看运行情况（第一次跑还挺慢的）
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/e3924ab2d53e4d4590a19e12e36e06df.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/8ab8adf45bee4eccb4f572fee40f9411.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/f55e1cc88d7a471288ea145b8b82b235.png)
然后这样就成功啦!
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/e0b4868c1dca435f98ff8cdbd161f4f5.png)

# 3、总结
jenkins部署vue的项目主要难点在node缺少glibc上（可能是node版本太高导致），在安装glibc2.28是尽量用gcc-8，之前安装过gcc-11去编译报错，后来说gcc-11不支持编译glibc-2.28。一般相互依赖的插件都会存在版本过低过高无法运行情况，尽量选择相对稳定的组合。

> 可能存在的问题，这个交给你们啦！哈哈哈哈～～